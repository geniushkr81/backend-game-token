const express = require('express');
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
swaggerDocument = require('./swagger.json');
var cors = require('cors');
const jwt = require('jsonwebtoken');
const config = require('./config/config');
// create express app
const app = express();
// Setup server port
const port = process.env.PORT || 5000;

app.set('key', config.key);
app.use(cors())
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse requests of content-type - application/json
app.use(bodyParser.json())
// define a root route
app.get('/', (req, res) => {
  res.send("Welcome API Game Backend");
});
// Require employee routes
//const employeeRoutes = require('./src/routes/employee.routes')
// using as middleware
//app.use('/api/v1/employees', employeeRoutes)

const protectedRoutes = express.Router();
protectedRoutes.use((req, res, next) => {
  const token = req.headers['access-token'];
  if(token){
    jwt.verify(token, app.get('key'), (err, decoded) => {
      if(err){
        return res.json({ mensaje: 'Token invalid'});
      }else{
        req.decoded = decoded;
        next();
      }
    })
  }else{
    res.send({
      mensaje: 'Token not provided'
    })
  }
})

// listen for requests
//const routerRoutes = require('./src/routes/routers.routes')
//const companiesRoutes = require('./src/routes/companies.routes')
//const locationsRoutes = require('./src/routes/locations.routes')
//const propertiesRoutes = require('./src/routes/properties.routes')
//const templatesRoutes = require('./src/routes/templates.routes')
//const publicityRoutes = require('./src/routes/publicity.routes')


//app.use('/routers', routerRoutes)
//app.use('/companies', companiesRoutes)
//app.use('/locations', locationsRoutes)
//app.use('/properties', propertiesRoutes)
//app.use('/templates', templatesRoutes)
//app.use('/publicity', publicityRoutes)
app.use('/docs',
  swaggerUi.serve,
  swaggerUi.setup(swaggerDocument)
);

app.listen(port, () => {
  console.log(`Server CORS is listening on port ${port}`);
});

app.post('/auth', (req, res) => {
  if(req.body.usuario === "admin" && req.body.contrasena === "admin") {
    const payload = {
      check: true
    };
    const token = jwt.sign(payload, app.get('key'), {
      expiresIn: 30000
    });
    res.json({
      mensaje: 'Authentication successful',
      token: token
    });
  }else{
    res.json({ mensaje: "Incorrect user or password"})
  }
})
