'use strict';
const Template = require('../models/templates.model');
exports.findAll = function(req, res) {
Template.findAll(function(err, template) {
  console.log('controller')
  if (err)
  res.send(err);
  console.log('res', template);
  res.send(template);
});
};
exports.create = function(req, res) {
const new_template = new Template(req.body);
//handles null error
if(req.body.constructor === Object && Object.keys(req.body).length === 0){
  res.status(400).send({ error:true, message: 'Please provide all required field' });
}else{
Template.create(new_template, function(err, template) {
  if (err)
  res.send(err);
  res.json({error:false,message:"Template added successfully!",data:template});
});
}
};
exports.findById = function(req, res) {
Template.findById(req.params.id, function(err, template) {
  if (err)
  res.send(err);
  res.json(template);
});
};
exports.update = function(req, res) {
  if(req.body.constructor === Object && Object.keys(req.body).length === 0){
    res.status(400).send({ error:true, message: 'Please provide all required field' });
  }else{
    Template.update(req.params.id, new Template(req.body), function(err, template) {
   if (err)
   res.send(err);
   res.json({ error:false, message: 'Template successfully updated' });
});
}
};
exports.delete = function(req, res) {
Template.delete( req.params.id, function(err, template) {
  if (err)
  res.send(err);
  res.json({ error:false, message: 'Template successfully deleted' });
});
};