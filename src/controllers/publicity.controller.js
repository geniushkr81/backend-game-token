'use strict';
const Publicity = require('../models/publicity.model');
exports.findAll = function(req, res) {
  Publicity.findAll(function(err, publicity) {
  console.log('controller')
  if (err)
  res.send(err);
  console.log('res', publicity); Publicity
  res.send(publicity);
});
};
exports.create = function(req, res) {
const new_publicity = new Publicity(req.body);
//handles null error
if(req.body.constructor === Object && Object.keys(req.body).length === 0){
  res.status(400).send({ error:true, message: 'Please provide all required field' });
}else{
Publicity.create(new_publicity, function(err, publicity) {
  if (err)
  res.send(err);
  res.json({error:false,message:"Publicity added successfully!",data:publicity});
});
}
};
exports.findById = function(req, res) {
  Publicity.findById(req.params.id, function(err, publicity) {
  if (err)
  res.send(err);
  res.json(publicity);
});
};
exports.update = function(req, res) {
  if(req.body.constructor === Object && Object.keys(req.body).length === 0){
    res.status(400).send({ error:true, message: 'Please provide all required field' });
  }else{
    Publicity.update(req.params.id, new Publicity(req.body), function(err, publicity) {
   if (err)
   res.send(err);
   res.json({ error:false, message: 'Publicity successfully updated' });
});
}
};
exports.delete = function(req, res) {
  Publicity.delete( req.params.id, function(err, publicity) {
  if (err)
  res.send(err);
  res.json({ error:false, message: 'Publicity successfully deleted' });
});
};