'use strict';
const Location = require('../models/locations.model');

// Create new location
exports.create = function(req, res) {
    const new_location = new Location(req.body);
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
      }else{
      Location.create(new_location, function(err, router) {
        if (err)
        res.send(err);
        res.json({error:false,message:"Location added successfully!",data:location});
      });
    }
}
// Get all locations
exports.findAll = function(req, res) {
    Location.findAll(function(err, location) {
      console.log('controller')
      if (err)
      res.send(err);
      console.log('res', location);
      res.send(location);
    });
    };

    exports.findById = function(req, res) {
        Location.findById(req.params.id, function(err, location) {
          if (err)
          res.send(err);
          res.json(location);
        });
        };
        exports.update = function(req, res) {
          if(req.body.constructor === Object && Object.keys(req.body).length === 0){
            res.status(400).send({ error:true, message: 'Please provide all required field' });
          }else{
            Location.update(req.params.id, new Location(req.body), function(err, location) {
           if (err)
           res.send(err);
           res.json({ error:false, message: 'Location successfully updated' });
        });
        }
        };
        exports.delete = function(req, res) {
        Location.delete( req.params.id, function(err, location) {
          if (err)
          res.send(err);
          res.json({ error:false, message: 'Location successfully deleted' });
        });
        };