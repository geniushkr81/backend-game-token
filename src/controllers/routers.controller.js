'use strict';
const Router = require('../models/routers.model');

// Create new router
exports.create = function(req, res) {
    const new_router = new Router(req.body);
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
      }else{
      Router.create(new_router, function(err, router) {
        if (err)
        res.send(err);
        res.json({error:false,message:"Router added successfully!",data:router});
      });
    }
}
// Get all routers
exports.findAll = function( req, res) {
    Router.findAll(function(err, router) {
      console.log('controller')
      if (err)
      res.send(err);
      console.log('res', router);
      res.send(router);
    });
    };
// Get all routers with parameters
exports.findAllrouters = function(req, res){
    Router.findAllrouters(function(err, router){
        console.log('controller')
        if(err)
        res.send(err);
        console.log('res', router);
        res.send(router);
    });
};

    exports.findById = function(req, res) {
        Router.findById(req.params.id, function(err, router) {
          if (err)
          res.send(err);
          res.json(router);
        });
        };
        exports.update = function(req, res) {
          if(req.body.constructor === Object && Object.keys(req.body).length === 0){
            res.status(400).send({ error:true, message: 'Please provide all required field' });
          }else{
            Router.update(req.params.id, new Router(req.body), function(err, router) {
           if (err)
           res.send(err);
           res.json({ error:false, message: 'Router successfully updated' });
        });
        }
        };
        exports.delete = function(req, res) {
        Router.delete( req.params.id, function(err, router) {
          if (err)
          res.send(err);
          res.json({ error:false, message: 'Router successfully deleted' });
        });
        };