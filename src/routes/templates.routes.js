const express = require('express')
const router = express.Router()
const templateController =   require('../controllers/templates.controller');

// Retrieve all 
router.get('/', templateController.findAll);
// Create a new 
router.post('/', templateController.create);
// Retrieve  with id
router.get('/:id', templateController.findById);
// Update with id
router.put('/:id', templateController.update);
// Delete with id
router.delete('/:id', templateController.delete);
module.exports = router