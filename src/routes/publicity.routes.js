const express = require('express')
const router = express.Router()
const publicityController =   require('../controllers/publicity.controller');

// Retrieve all 
router.get('/', publicityController.findAll);
// Create a new 
router.post('/', publicityController.create);
// Retrieve  with id
router.get('/:id', publicityController.findById);
// Update with id
router.put('/:id', publicityController.update);
// Delete with id
router.delete('/:id', publicityController.delete);
module.exports = router