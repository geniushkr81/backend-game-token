const express = require('express')
const location = express.Router()
const locationController = require('../controllers/locations.controller');
// Locations
location.post('/', locationController.create);

// All 
location.get('/', locationController.findAll);
// Retrieve a single  with id
location.get('/:id', locationController.findById);
// Update  with id
location.put('/:id', locationController.update);
// Delete  with id
location.delete('/:id', locationController.delete);

module.exports = location