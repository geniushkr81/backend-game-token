const express = require('express')
const router = express.Router()
const routerController = require('../controllers/routers.controller');
// ROUTERS
router.post('/', routerController.create);

// All routers
router.get('/', routerController.findAll);
// all routers parameters
router.get('/all',routerController.findAllrouters);
// Retrieve a single  with id
router.get('/:id', routerController.findById);
// Update  with id
router.put('/:id', routerController.update);
// Delete  with id
router.delete('/:id', routerController.delete);

module.exports = router