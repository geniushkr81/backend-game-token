const express = require('express')
const router = express.Router()
const propertyController =   require('../controllers/properties.controller');

// Retrieve all 
router.get('/', propertyController.findAll);
// Create a new 
router.post('/', propertyController.create);
// Retrieve  with id
router.get('/:id', propertyController.findById);
// Update with id
router.put('/:id', propertyController.update);
// Delete with id
router.delete('/:id', propertyController.delete);
module.exports = router