const express = require('express')
const router = express.Router()
const companyController =   require('../controllers/companies.controller');

// Retrieve all 
router.get('/', companyController.findAll);
// Create a new 
router.post('/', companyController.create);
// Retrieve  with id
router.get('/:id', companyController.findById);
// Update with id
router.put('/:id', companyController.update);
// Delete with id
router.delete('/:id', companyController.delete);
module.exports = router