'use strict';
var dbConn = require('../../config/db.config');
//Property object create
var Property = function(property){
  this.id             = property.id;
  this.name           = property.name;
  this.location_id    = property.location_id;
  this.status         = property.status ? property.status : 1;
  this.created_at     = new Date();
  this.updated_at     = new Date();
};

Property.create = function (newProp, result) {
dbConn.query("INSERT INTO properties set ?", newProp, function (err, res) {
if(err) {
  console.log("error: ", err);
  result(err, null);
}
else{
  console.log(res.insertId);
  result(null, res.insertId);
}
});
};
Property.findById = function (id, result) {
dbConn.query("Select * from properties where id = ? ", id, function (err, res) {
if(err) {
  console.log("error: ", err);
  result(err, null);
}
else{
  result(null, res);
}
});
};
Property.findAll = function (result) {
dbConn.query("Select * from properties", function (err, res) {
if(err) {
  console.log("error: ", err);
  result(null, err);
}
else{
  console.log('properties : ', res);
  result(null, res);
}
});
};
Property.update = function(id, property, result){
dbConn.query("UPDATE properties SET name=?,location_id=?,status=?,update_at=? WHERE id = ?", [property.name,property.location_id,property.status,property.updated_at ,id], function (err, res) {
if(err) {
  console.log("error: ", err);
  result(null, err);
}else{
  result(null, res);
}
});
};
Property.delete = function(id, result){
dbConn.query("DELETE FROM properties WHERE id = ?", [id], function (err, res) {
if(err) {
  console.log("error: ", err);
  result(null, err);
}
else{
  result(null, res);
}
});
};
module.exports= Property;