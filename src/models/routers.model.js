var dbConn = require('./../../config/db.config');

// Routers object
var Router = function(router){
    this.id             = router.id;
    this.company_id     = router.company_id;
    this.location_id    = router.location_id;
    this.property_id    = router.property_id;
    this.status         = router.status ? router.status : 1;
    // this.last_connection     = Date();
    this.created_at     = new Date();
    this.update_at      = Date();
};
Router.create = function(newRouter, result){
    dbConn.query("INSERT INTO routers SET ?", newRouter, function(err, res){
        if(err){
            console.log("error: ", err);
            result(err, null);
        }else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
}
Router.findById = function (id, result) {
    dbConn.query("Select * from routers where id = ? ", id, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(err, null);
    }
    else{
      result(null, res);
    }
    });
    };

    Router.findAllrouters = function (result) {
        dbConn.query("SELECT r.Id as id, r.name as name, r.status, DATE_FORMAT(MAX(lv.last_connection),'%d-%m-%Y %H:%i:%s') as last_connection, p.name as property, l.name as location, c.name as company from routers AS r INNER JOIN live_log as lv ON lv.router_id = r.id INNER JOIN properties_routers as pr ON pr.router_id = r.id INNER JOIN properties AS p on p.id = pr.property_id INNER JOIN locations_properties as lp ON lp.property_id = p.id INNER JOIN locations as l on l.id = lp.location_id INNER JOIN companies_locations as cl ON cl.company_id = l.id INNER JOIN companies as c ON c.id = cl.company_id group by r.Id",
        function(err,res) {
            if(err){
                console.log("error: ", err);
                result(null,err);
            }else{
                console.log('routers : ', res);
                result(null, res);
            }
        });

    }

    Router.findAll = function (result) {
    dbConn.query("Select * from routers", function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      console.log('routers : ', res);
      result(null, res);
    }
    });
    };
    Router.update = function(id, router, result){
    dbConn.query("UPDATE routers SET status=?,update_at=? WHERE id = ?", [router.status,router.update_at,router.id], function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }else{
      result(null, res);
    }
    });
    };
    Router.delete = function(id, result){
    dbConn.query("DELETE FROM routers WHERE id = ?", [id], function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
    });
    };
module.exports=Router;