'use strict';
var dbConn = require('./../../config/db.config');
//Company object create
var Company = function(company){
  this.id             = company.id;
  this.name           = company.name;
  this.template_id    = company.template_id;
  this.publicity_id   = company.publicity_id;
  this.status         = company.status ? company.status : 1;
  this.created_at     = new Date();
  this.updated_at     = new Date();
};

Company.create = function (newCom, result) {
dbConn.query("INSERT INTO companies set ?", newCom, function (err, res) {
if(err) {
  console.log("error: ", err);
  result(err, null);
}
else{
  console.log(res.insertId);
  result(null, res.insertId);
}
});
};
Company.findById = function (id, result) {
dbConn.query("Select * from companies where id = ? ", id, function (err, res) {
if(err) {
  console.log("error: ", err);
  result(err, null);
}
else{
  result(null, res);
}
});
};
Company.findAll = function (result) {
dbConn.query("Select * from companies", function (err, res) {
if(err) {
  console.log("error: ", err);
  result(null, err);
}
else{
  console.log('companies : ', res);
  result(null, res);
}
});
};
Company.update = function(id, company, result){
dbConn.query("UPDATE companies SET name=?,template_id=?,publicity_id=?,status=?,update_at=? WHERE id = ?", [company.name,company.template_id,company.publicity_id,company.status,company.updated_at ,id], function (err, res) {
if(err) {
  console.log("error: ", err);
  result(null, err);
}else{
  result(null, res);
}
});
};
Company.delete = function(id, result){
dbConn.query("DELETE FROM companies WHERE id = ?", [id], function (err, res) {
if(err) {
  console.log("error: ", err);
  result(null, err);
}
else{
  result(null, res);
}
});
};
module.exports= Company;