var dbConn = require('./../../config/db.config');

// Template object
var Template = function(template){
    this.id             = template.id;
    this.name           = template.name;
    this.data           = template.data;
    this.status         = template.status ? template.status : 1;
    this.created_at     = new Date();
    this.update_at      = new Date();
};
Template.create = function(newTemplate, result){
    dbConn.query("INSERT INTO template SET ?", newTemplate, function(err, res){
        if(err){
            console.log("error: ", err);
            result(err, null);
        }else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
}
Template.findById = function (id, result) {
    dbConn.query("Select * from template where id = ? ", id, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(err, null);
    }
    else{
      result(null, res);
    }
    });
    };
    Template.findAll = function (result) {
    dbConn.query("Select * from template", function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      console.log('templates : ', res);
      result(null, res);
    }
    });
    };
    Template.update = function(id, template, result){
    dbConn.query("UPDATE template SET name=?,data=?,status=?,update_at=? WHERE id = ?", [template.name, tmeplate.data,template.status,template.update_at,template.id], function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }else{
      result(null, res);
    }
    });
    };
    Template.delete = function(id, result){
    dbConn.query("DELETE FROM template WHERE id = ?", [id], function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
    });
    };
module.exports=Template;