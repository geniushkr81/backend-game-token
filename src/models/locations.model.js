var dbConn = require('./../../config/db.config');

// Locations object
var Location = function(location){
    this.id             = location.id;
    this.name           = location.name;
    this.status         = location.status ? location.status : 1;
    this.created_at     = new Date();
    this.update_at      = new Date();
};
Location.create = function(newLocation, result){
    dbConn.query("INSERT INTO locations SET ?", newLocation, function(err, res){
        if(err){
            console.log("error: ", err);
            result(err, null);
        }else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
}
Location.findById = function (id, result) {
    dbConn.query("Select * from locations where id = ? ", id, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(err, null);
    }
    else{
      result(null, res);
    }
    });
    };
    Location.findAll = function (result) {
    dbConn.query("Select * from locations", function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      console.log('routers : ', res);
      result(null, res);
    }
    });
    };
    Location.update = function(id, location, result){
    dbConn.query("UPDATE locations SET status=?,update_at=? WHERE id = ?", [location.status,location.update_at,location.id], function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }else{
      result(null, res);
    }
    });
    };
    Location.delete = function(id, result){
    dbConn.query("DELETE FROM locations WHERE id = ?", [id], function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
    });
    };
module.exports=Location;