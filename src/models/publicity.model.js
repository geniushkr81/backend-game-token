var dbConn = require('./../../config/db.config');

// Publicity object
var Publicity = function(publicity){
    this.id             = publicity.id;
    this.image          = publicity.image;
    this.status         = publicity.status ? publicity.status : 1;
    this.created_at     = new Date();
    this.update_at      = new Date();
};
Publicity.create = function(newPublicity, result){
    dbConn.query("INSERT INTO publicity SET ?", newPublicity, function(err, res){
        if(err){
            console.log("error: ", err);
            result(err, null);
        }else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
}
Publicity.findById = function (id, result) {
    dbConn.query("Select * from publicity where id = ? ", id, function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(err, null);
    }
    else{
      result(null, res);
    }
    });
    };
    Publicity.findAll = function (result) {
    dbConn.query("Select * from publicity", function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      console.log('publicity : ', res);
      result(null, res);
    }
    });
    };
    Publicity.update = function(id, publicity, result){
    dbConn.query("UPDATE publicity SET image=?,status=?,update_at=? WHERE id = ?", [publicity.image,publicity.status,publicity.update_at,publicity.id], function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }else{
      result(null, res);
    }
    });
    };
    Publicity.delete = function(id, result){
    dbConn.query("DELETE FROM publicity WHERE id = ?", [id], function (err, res) {
    if(err) {
      console.log("error: ", err);
      result(null, err);
    }
    else{
      result(null, res);
    }
    });
    };
module.exports=Publicity;